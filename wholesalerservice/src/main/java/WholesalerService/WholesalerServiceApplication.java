package WholesalerService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WholesalerServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(WholesalerServiceApplication.class, args);
	}

}
