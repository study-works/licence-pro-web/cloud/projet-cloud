package fr.iut.heroku.exception;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * Thrown when book ISBN is not valid
 */
@Provider
public class UnknownBookException extends Exception implements ExceptionMapper<UnknownBookException> {

    public UnknownBookException(){
        super("ISBN book is not valid.");
    }

    public UnknownBookException(String isbn){
        super("The isbn "+isbn+" is not a valid ISBN.");
    }

    @Override
    public Response toResponse(UnknownBookException e) {
        System.out.println("UnknownBookException");
        return Response.status(404).entity(e.getMessage()).type(MediaType.TEXT_PLAIN).build();
    }
}
