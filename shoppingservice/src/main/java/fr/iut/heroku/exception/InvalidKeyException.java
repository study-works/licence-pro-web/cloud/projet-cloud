package fr.iut.heroku.exception;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * Thrown when Wholeseler must be used but the keypass is not valid
 */
@Provider
public class InvalidKeyException extends Exception implements ExceptionMapper<InvalidKeyException> {
    public InvalidKeyException(){
        super("Your are not allowed to replenish stocks.");
    }

    public InvalidKeyException(int key){
        super("The key : "+key+" is not a valid key. Your are not allowed to replenish stocks.");
    }

    @Override
    public Response toResponse(InvalidKeyException e) {
        System.out.println("InvalidKeyException");
        return Response.status(403).entity(e.getMessage()).type(MediaType.TEXT_PLAIN).build();
    }
}
