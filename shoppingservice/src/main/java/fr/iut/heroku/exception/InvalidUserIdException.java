package fr.iut.heroku.exception;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class InvalidUserIdException extends Exception implements ExceptionMapper<InvalidUserIdException> {

    public InvalidUserIdException(){
        super("The user id is not a valid id.");
    }

    public InvalidUserIdException(String id){
        super("The user id "+id+" is not a valid id.");
    }

    @Override
    public Response toResponse(InvalidUserIdException e) {
        System.out.println("InvalidUserIdException");
        return Response.status(403).entity(e.getMessage()).type(MediaType.TEXT_PLAIN).build();
    }
}
