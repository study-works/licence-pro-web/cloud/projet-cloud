package fr.iut.heroku.exception;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class UserNotAddedException extends Exception implements ExceptionMapper<UserNotAddedException> {

    public UserNotAddedException(){
        super("The new user was not added because the length was wrong.");
    }

    public UserNotAddedException(String id){
        super("The user "+id+" was not added because the length was wrong.");
    }

    @Override
    public Response toResponse(UserNotAddedException e) {
        System.out.println("UserNotAddedException");
        return Response.status(400).entity(e.getMessage()).type(MediaType.TEXT_PLAIN).build();
    }
}
