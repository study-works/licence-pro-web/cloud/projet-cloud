package fr.iut.heroku.controller;

import fr.iut.heroku.exception.InvalidKeyException;
import fr.iut.heroku.exception.InvalidUserIdException;
import fr.iut.heroku.exception.UnknownBookException;
import fr.iut.heroku.exception.UserNotAddedException;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.logging.LoggingFeature;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.sql.*;


@Path("stock")
public class StockController {

    private Connection getConnection() throws Exception {
        // Class.forName("org.postgresql.Driver");
        URI dbUri = new URI(System.getenv("DATABASE_URL"));

        String username = dbUri.getUserInfo().split(":")[0];
        String password = dbUri.getUserInfo().split(":")[1];
        String dbUrl = "jdbc:postgresql://" + dbUri.getHost() + dbUri.getPath();

        return DriverManager.getConnection(dbUrl, username, password);
    }

    @GET
    @Path("addUser")
    @Produces(MediaType.TEXT_PLAIN)
    public String addUser(@QueryParam("id") String userId){ //le return String permet d'afficher
        try{
            Connection connection = getConnection();
            Statement stmt = connection.createStatement();



            stmt.executeUpdate("CREATE TABLE IF NOT EXISTS users (id VARCHAR(3) UNIQUE)");
            if(userId.length() != 3){
                throw new UserNotAddedException(userId);
            }

            stmt.executeUpdate("INSERT INTO users VALUES ("+userId+")");

            ResultSet rs = stmt.executeQuery("SELECT * FROM users");
            String out = displayUsersInDatabase();
            return out;

        } catch (Exception e){
            return "There was an error when you added a user : " + e.getMessage();
        }
    }

    @GET
    @Path("displayUsers")
    @Produces(MediaType.TEXT_PLAIN)
    public String displayUsersInDatabase(){
        try {
            Connection connection = getConnection();
            Statement stmt = connection.createStatement();

            ResultSet rs = stmt.executeQuery("SELECT * FROM users");
            String out = "Users in database : \n";
            while (rs.next()){
                out += "User : "+rs.getString("id")+"\n";
            }
            return out;
        } catch (Exception e) {
            return "There was an error when you displayed a user : " + e.getMessage();
        }
    }

    @GET
    @Path("deleteUsers")
    @Produces(MediaType.TEXT_PLAIN)
    public String deleteUsersFromDatabase(){
        try {
            Connection connection = getConnection();
            Statement stmt = connection.createStatement();

            stmt.executeQuery("DELETE FROM users");
            return "All users have been removed";
        } catch (Exception e){
            return "There was an error when you removed users : " + e.getMessage();
        }
    }

    @GET
    @Path("dropTableUsers")
    @Produces(MediaType.TEXT_PLAIN)
    public String deleteTableUsers(){
        try {
            Connection connection = getConnection();
            Statement stmt = connection.createStatement();

            stmt.executeQuery("DROP TABLE users");
            return "Table users removed";
        } catch (Exception e){
            return "There was an error when you removed the table users : " + e.getMessage();
        }
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public Boolean isUser(@Context String userId){
        try {
            Connection connection = getConnection();
            Statement stmt = connection.createStatement();

            ResultSet rs = stmt.executeQuery("SELECT * FROM users WHERE id="+userId+"::varchar(255)");

            if(!rs.next()){
                throw new InvalidUserIdException(userId);
            }

            String out = "User data : \n";
            while (rs.next()){
                out += "User : "+rs.getString("id")+"\n";
            }
            return true;
        } catch (Exception e){
            e.getMessage();
            return false;
        }
    }

    @GET
    @Path("/displaySingleUser")
    @Produces(MediaType.TEXT_PLAIN)
    public String displaySingleUser(@QueryParam("id") String userId){
        try {
            Connection connection = getConnection();
            Statement stmt = connection.createStatement();

            ResultSet rs = stmt.executeQuery("SELECT * FROM users WHERE id="+userId+"::varchar(255)");
            String out = "User data : \n";
            while (rs.next()){
                out += "User : "+rs.getString("id")+"\n";
            }
            return out;
        } catch (Exception e){
            return "Impossible to read this user : " + e.getMessage();
        }
    }

    /**
     * Allows to see a book stock
     * @param isbn : Book ISBN
     * @return : the request result from StockService
     */
    @GET
    @Path("/getStock/{isbn}")
    @Produces(MediaType.TEXT_PLAIN)
    public Response getStockFromStockService(@PathParam("isbn") String isbn){
        Client client = ClientBuilder.newClient(new ClientConfig());
        WebTarget webTarget = client.target("https://lpro-cloud-stock-service.oa.r.appspot.com").path("getStock").queryParam("isbn", isbn);
        Invocation.Builder invocationBuilder = webTarget.request();
        Response response = invocationBuilder.get();
        return response;
    }

    /**
     * Allows to check if the ISBN is valid.
     * If it is, it will allows to order books by the way of the WholeselerService.
     * This is the only method called by customers.
     * @param isbn : Book ISBN
     * @param quantity : Desired book quantity
     * @param key : Allows to call the WholeselerService if quantity > stock
     * @return : Updated stock
     * @throws UnknownBookException
     * @throws InvalidKeyException
     */
    @GET
    @Path("/buy")
    @Produces(MediaType.TEXT_PLAIN)
    public Response bookRequest(@QueryParam("id") String userId, @QueryParam("isbn") String isbn, @QueryParam("quantity") String quantity, @QueryParam("key") String key) throws UnknownBookException, InvalidKeyException, InvalidUserIdException {
        if(!isUser(userId)){
            throw new InvalidUserIdException(userId);
        }

        long corr = Long.parseLong(userId+isbn);

        Client stockService = ClientBuilder.newClient(new ClientConfig());
        WebTarget webTargetStockService = stockService.target("https://lpro-cloud-stock-service.oa.r.appspot.com").path("getStock").queryParam("isbn", isbn).queryParam("correlation", corr);
        Invocation.Builder invocationBuilderStockService = webTargetStockService.request();
        Response quantityResponse = invocationBuilderStockService.get();
        if(quantityResponse.getStatus() != 200) {
            throw new UnknownBookException(isbn);
        }

        //ISBN is valid
        buyRequest(isbn, Integer.parseInt(quantity), Integer.parseInt(key), corr);

        Response quantityResponseAfterBR = invocationBuilderStockService.get(); //Allows to return the updated stock

        return quantityResponseAfterBR;
    }

    /**
     * Allows to order books.
     * @param isbn : Book ISBN
     * @param quantity : Desired book quantity
     * @param key : Allows to call the WholeselerService if quantity > stock
     * @throws InvalidKeyException
     */
    @GET
    @Consumes(MediaType.TEXT_PLAIN)
    public void buyRequest(@Context String isbn, @Context int quantity, @Context int key, @Context long corr) throws InvalidKeyException {
        Client stockService = ClientBuilder.newClient(new ClientConfig());
        WebTarget webTargetStockService = stockService.target("https://lpro-cloud-stock-service.oa.r.appspot.com").path("reduceStock").queryParam("isbn", isbn).queryParam("amount", quantity).queryParam("correlation", corr);
        Invocation.Builder invocationBuilderStockService = webTargetStockService.request();
        Response updatedQuantity = invocationBuilderStockService.get();

        if(updatedQuantity.getStatus() != 200) { //When desired quantity > book stock, WholeselerService is call
            Client wholeselerService = ClientBuilder.newClient(new ClientConfig());
            WebTarget webTargetWholeselerService = stockService.target("https://lpro-cloud-wholesale-service.ew.r.appspot.com").path("wholesaleBook").queryParam("key", key).queryParam("isbn", isbn).queryParam("amount", quantity).queryParam("correlation", corr);
            Invocation.Builder invocationBuilderWholeselerService = webTargetWholeselerService.request();
            Response increasedQuantity = invocationBuilderWholeselerService.get();
            if(increasedQuantity.getStatus() != 200){
                throw new InvalidKeyException(key);
            }
            Response quantityAfterWholeseler = invocationBuilderStockService.get();
        }

    }



}
