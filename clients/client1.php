<?php

require 'vendor/autoload.php';
use GuzzleHttp\Client;


$client = new Client([
    'base_uri' => 'https://evening-mesa-38265.herokuapp.com',
    'timeout'  => 100,
]);


$response = $client->request('GET', '/stock/buy',
    [
        'query' => [
                'id' => '123',
                'isbn' => '12344',
                'quantity' => '3',
                'key' => '4589'
            ]
    ]
);

$response = json_decode($response->getBody(), true);


$book = $response['book'];
echo "|----- Book -----|\n";
echo "|- ISBN: " . $book["isbn"] . "\n";
echo "|- Title: " . $book["title"] . "\n";
echo "|- Author: " . $book["author"] . "\n";
echo "|- Synopsis: " . $book["synopsis"] . "\n";
echo "|- Amount: " . $book["amount"] . "\n";

?>