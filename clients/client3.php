<?php
require 'vendor/autoload.php';
use GuzzleHttp\Client;

$quantity = 7;

$client = new Client([
	'base_uri' => 'https://evening-mesa-38265.herokuapp.com',
	'timeout'  => 100,
]);

$get = $client->request('GET', '/stock/buy', [
	'query' => [
		'id'       => '456',
		'isbn'     => '5644004762845184',
		'quantity' => $quantity,
		'key'      => '4589',
	],
]);

$response = json_decode($get->getBody(), true);

echo "Your order of " . $quantity . " books has been processed.\n";

$book = $response['book'];
echo "|----- Book -----|\n";
echo "|- ISBN: " . $book["isbn"] . "\n";
echo "|- Title: " . $book["title"] . "\n";
echo "|- Author: " . $book["author"] . "\n";
echo "|- Synopsis: " . $book["synopsis"] . "\n";
echo "|----------------|\n";

