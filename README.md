# LAHOUEL Thomas - BRUGIÈRE Gaëthan
Dans ce projet, nous avions comme objectif de développer et déployer, sur les plateformes Heroku et Google App Engine (GAE), 3 services communiquant les uns avec les autres.\
À travers ces 3 services, nous avions pour but de gérer des commandes de livres en utilisant au moins une base de données.

Vous pouvez accéder au repository Gitlab à l'URL suivante : https://gitlab.iut-clermont.uca.fr/cloud/projet/projet-cloud ou cloner le projet avec https://gitlab.iut-clermont.uca.fr/cloud/projet/projet-cloud.git

## Services
### Shopping Service
**Déploiement** : Heroku\
**Création** : Thomas

Le Shopping Service est appelé par des clients pour connaître l’état de stock de livres et les commander s’ils sont disponibles.
C'est donc par ce service que passent toutes les requêtes. En fonction de la requête, ce service fait appel au Stock Service ou au Wholesaler Service.\
Lorsque ce service est appelé, il faut préciser l'identifiant de l'utilisateur souhaitant passer une commande, l'ISBN du livre que l'utilisateur souhaite commander, la quantité souhaitée et une clé valide afin de faire appel au Wholesaler dans le cas où le stock ne serait pas suffisant.\
Ce service comprend également la gestion d'erreurs.

### Stock Service
**Déploiement** : GAE\
**Création** : Gaëthan

Le Stock Service permet de gérer le stock des libres au sein de la base de donnée. Il est ainsi constitué de 3 méthodes permettant respectivement de se renseigner sur le stock pour un livre donné, d'augmenter le stock d'un livre, de diminuer le stock d'un livre.\
La première méthode prend en paramètre l'ISBN du livre ainsi qu'un identifiant de corrélation. Il renvoie ensuite cet identifiant ainsi que le livre complet au format JSON.\
La méthode permettant d'augmenter le stock prend un paramètre en plus, qui est la quantité à ajouter.\
La méthode permettant de diminuer le stock va, elle, prendre en plus la quantité de livre à enlever du stock.\
Ce service comprend également la gestion d'erreurs.

### Wholesaler Service
**Déploiement** : GAE\
**Création** : Gaëthan

Le Wholesaler Service permet de commander des livres au besoin.
Il permet ainsi d'augmenter le stock si la commande est supérieure au stock.\
L'unique méthode composant le service prend en paramètre une clé de sécurité, comparée à une constante dans le code. Elle prend aussi l'ISBN, la quantité de livre dont il y a besoin, et un identifiant de corrélation.\
Pour toute quantité demandée, le service fera une commande de livre d'une quantité égal au multiple de 5 supérieur le plus proche (ex: qte demandée = 3 ==> qte commandée = 5).\
Ce service comprend également la gestion d'erreurs.

## Base de données
### Livres et gestion des stocks
**Emplacement** : GAE\
**Utilisation** : Stock Service et Wholesaler Service\
**Création** : Gaëthan

Cette base de données permet de stocker les livres et leur stock.
C'est Gaëthan qui s'est occupé de créer cette base de données.

### Utilisateurs
**Emplacement** : Heroku\
**Utilisation** : Shopping Service\
**Création** : Thomas

La base permet de gérer les identifiants des utilisateurs souhaitant passer une commande.

## Clients Guzzle
Nous avons développé 3 clients PHP avec Guzzle dans le but de simuler des accès au Shopping Service.\
- Les clients 1 et 2 ont été développés par Thomas.
- Le 3ᵉ client a été développé par Gaëthan.

## URLs du projet

Afficher les utilisateurs en base de données :
- https://evening-mesa-38265.herokuapp.com/stock/displayUsers

Passer une commande :
- https://evening-mesa-38265.herokuapp.com/stock/buy?id=123&ISBN=12344&quantity=3&key=4589