package StockService.model;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

@Entity
public class Book {
    @Id Long isbn;
    String title;
    String author;
    String synopsis;
    int amount;

    private Book() {}

    public Book(String title, String author, String synopsis, int amount) {
        this.title = title;
        this.author = author;
        this.synopsis = synopsis;
        this.amount = amount;
    }

    public Book(Long isbn, String title, String author, String synopsis, int amount) {
        this.isbn = isbn;
        this.title = title;
        this.author = author;
        this.synopsis = synopsis;
        this.amount = amount;
    }

    public Long getIsbn() {
        return isbn;
    }

    public void setIsbn(Long isbn) {
        this.isbn = isbn;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getSynopsis() {
        return synopsis;
    }

    public void setSynopsis(String synopsis) {
        this.synopsis = synopsis;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
