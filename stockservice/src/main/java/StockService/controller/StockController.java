package StockService.controller;

import static com.googlecode.objectify.ObjectifyService.ofy;

import StockService.exception.BookNotFoundException;
import StockService.exception.BookStockToSmallException;
import StockService.model.Book;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
public class StockController {

    @GetMapping(value = "/getStock", produces = "application/json")
    public @ResponseBody Map<String, Object> getStock(@RequestParam(value="isbn") Long isbn, @RequestParam(value="correlation") Long correlation) {
        Map<String, Object> responseBody = new HashMap<>();

        Book book = ofy().load().type(Book.class).id(isbn).now();

        if (book == null) {
            throw new BookNotFoundException("ISBN does not exist!");
        }

        responseBody.put("book", book);
        responseBody.put("correlation", correlation);

        return responseBody;
    }

    @GetMapping(value = "/increaseStock", produces = "application/json")
    public @ResponseBody Map<String, Object> increaseStock(@RequestParam(value="isbn") Long isbn, @RequestParam(value="amount") int amount, @RequestParam(value="correlation") Long correlation) {
        Map<String, Object> responseBody = new HashMap<>();

        Book book = ofy().load().type(Book.class).id(isbn).now();

        if (book == null) {
            throw new BookNotFoundException("ISBN does not exist!");
        }

        book.setAmount(book.getAmount() + amount);
        ofy().save().entity(book).now();

        responseBody.put("book", book);
        responseBody.put("correlation", correlation);

        return responseBody;
    }

    @GetMapping(value = "/reduceStock", produces = "application/json")
    public @ResponseBody Map<String, Object> reduceStock(@RequestParam(value="isbn") Long isbn, @RequestParam(value="amount") int amount, @RequestParam(value="correlation") Long correlation) {
        Map<String, Object> responseBody = new HashMap<>();

        Book book = ofy().load().type(Book.class).id(isbn).now();

        if (book == null) {
            throw new BookNotFoundException("ISBN does not exist!");
        }

        int oldAmount = book.getAmount();

        if (oldAmount < amount) {
            throw new BookStockToSmallException("The stock is to small to reduce it!");
        }

        book.setAmount(book.getAmount() - amount);
        ofy().save().entity(book).now();

        responseBody.put("book", book);
        responseBody.put("correlation", correlation);

        return responseBody;
    }
}
