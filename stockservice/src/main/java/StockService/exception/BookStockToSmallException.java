package StockService.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "The stock is to small")
public class BookStockToSmallException extends RuntimeException {
    public BookStockToSmallException(String msg) {
        super(msg);
    }
}
